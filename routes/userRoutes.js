const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController")
const auth = require("../auth")

router.post("/register", (req,res) => {
	userController.registerUsers(req.body).then(resultFromController => res.send(resultFromController))
})

router.post("/check", (req,res) => {
	userController.checkEmail(req.body).then(resultFromController => res.send(resultFromController))
});

router.post("/login", (req,res) => {
	userController.logInUser(req.body).then(resultFromController => res.send(resultFromController))
})

router.get("/details", auth.verify, (req,res) => {
	const userData = auth.decode(req.headers.authorization);
	console.log(userData)

	userController.info({userId: userData.id}).then(resultFromController => res.send(resultFromController))
});

router.put("/:userId/setAsAdmin", auth.verify, (req,res) => {
	const data = {
		userId: req.params.userId,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	console.log(data)
	console.log("CApsdfgbvisfVJV")
	userController.setAsAdmin(data).then(resultFromController => {
		console.log(resultFromController)
		res.send(resultFromController)})
});

router.post("/order", auth.verify, (req,res) => {
	let data = {
		userId: auth.decode(req.headers.authorization).id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		productId: req.body.productId
	}

	if(data.isAdmin){
		res.send ("Non-Admin can create orders")
	} else {
		userController.order(data).then(resultFromController => res.send(resultFromController))
	}
});

/*router.get("/details", auth.verify, (req,res) => {
	const userData = auth.decode(req.headers.authorization);
	console.log(userData)

	userController.info({userId: userData.id}).then(resultFromController => res.send(resultFromController))
});*/

router.get("/myOrders", auth.verify, (req,res) => {
	const userOrders = auth.decode(req.headers.authorization)
	console.log(userOrders)
	if(userOrders.isAdmin){
		res.send("Only non admins can see this.")
	} else {
		userController.getMyOrders(userOrders.id).then(resultFromController => res.send(resultFromController))
	}
})

module.exports = router;