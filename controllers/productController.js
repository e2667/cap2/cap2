const product = require("../models/Product");
const auth = require("../auth");

module.exports.addNewProduct = (admin, reqBody) => {
	let newProduct = new product({
		productName: reqBody.productName,
		description: reqBody.description,
		price: reqBody.price,
	})
	return newProduct.save().then((product, error) => {
		if(error){
			return false
		} else {
			return product
		}
	})
} 

module.exports.updateProduct = (data) => {
	console.log(data)
	return product.findById(data.productId).then((result, error) => {
		console.log(result)
		if(data.isAdmin){
			result.name = data.updatedProduct.name
			result.description = data.updatedProduct.description
			result.price = data.updatedProduct.price
			console.log(result)
			return result.save().then((updatedProduct, error) => {
				if(error){
					return false
				} else {
					return updatedProduct
				}
			})
		} else {
			return "Not Admin"
		}
	})
}

module.exports.getAllActiveProducts = () => {
	return product.find({isActive: true}).then(result => {
		return result
	})
}

module.exports.getProduct = (reqParams) => {
	return product.findById(reqParams.productId).then(result => {
		return result
	})
}

module.exports.archiveProduct = (data) => {
	console.log(data)
	return product.findById(data.productId).then((result,error) => {
		console.log(result)
		if(data.isAdmin) {
			result.isActive = false

			return result.save().then((updatedProduct, error) => {
				if(error){
					return false
				} else {
					return updatedProduct
				}
			})
		} else {
			return "Not Admin"
		}
	})
}

module.exports.getAllOrders = (data) => {
	return product.findById(data.productId).then((result,error) => {
		console.log(result)
		if(data.isAdmin){
			return result
		} else {
			return "Not an Admin"
		}
	})
}

