const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController")
const auth = require("../auth");

router.post("/add", auth.verify, (req,res) => {
	const userRole = auth.decode(req.headers.authorization);
	console.log(userRole.isAdmin);

	if(userRole.isAdmin){
		productController.addNewProduct(userRole.isAdmin,req.body).then(resultFromController => res.send(resultFromController))
	} else {
		res.send("sorry")
	}
	
})

router.put("/:productId", auth.verify, (req,res) => {
	const data = {
		productId: req.params.productId,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		updatedProduct: req.body
	}
	productController.updateProduct(data).then(resultFromController => res.send(resultFromController))

})

router.get("/all", auth.verify, (req,res) => {
	productController.getAllActiveProducts().then(resultFromController => res.send(resultFromController))
})

router.get("/:productId", (req,res) => {
	console.log(req.params.productId)

	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController))
})

router.put("/:productId/archive", auth.verify, (req,res) => {
	const data = {
		productId: req.params.productId,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	productController.archiveProduct(data).then(resultFromController => res.send(resultFromController))
})

router.get("/:productId/allOrders", (req,res) => {
	let data = {
		productId: req.params.productId,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	productController.getAllOrders(data).then(resultFromController => res.send(resultFromController))
})



module.exports = router;