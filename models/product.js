const mongoose = require("mongoose")

const productSchema = new mongoose.Schema({
	productName: {
		type: String,
		required: true
	},
	description: {
		type: String,
		required: true
	},
	price: {
		type: Number,
		required: true
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
	orderee: [
		{
			userId: {
				type: String,
				required: true
			},
			orderedOn: {
				type: Date,
				default: new Date()
			}
		}
	]
});

module.exports = mongoose.models.Product || mongoose.model("Product", productSchema)