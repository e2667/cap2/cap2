 const user = require("../models/user");
const product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth");

module.exports.registerUsers = (reqBody) => {
	let newUser = new user({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10)
	})
	return newUser.save().then((user, error) => {
		if(error){
			return false
		} else {
			return true
		}
	})
}


module.exports.checkEmail = (reqBody) => {
	return user.find({email: reqBody.email}).then(result => {
		if(result.length > 0){
			return true
		} else {
			return false
		}
	})
};

module.exports.logInUser = (reqBody) => {
	return user.findOne({email: reqBody.email}).then(result => {
		if(result == null){
			return false
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
			if(isPasswordCorrect){
				return {access: auth.createAccessToken(result)}
			} else {
				return false
			}
		}
	})
}

module.exports.info = (data) => {
	return user.findById(data.userId).then((result, error) => {
		if(error){
			return false
		} else {
			result.password = ""
			return result
		}
	})
};

module.exports.setAsAdmin = (data) => {
	return user.findById(data.userId).then((result, error) => {
		
		if(!data.isAdmin){
			return false
		}

		if(result.isAdmin){
			return "User is already an admin"
		} else {
			result.isAdmin = true
			return result.save().then((updatedUser, error) => {
				if(error){
					return false
				} else {
					return updatedUser
				}
			})
		}
	})
};

module.exports.order = async (data) => {
	console.log(data)

	let isUserUpdated = await user.findById(data.userId).then(user => {
		user.orders.push({productId: data.productId});
		return user.save().then((user, error) => {
			if(error){
				return false
			} else {
				return true
			}
		})
	})

	let isProductUpdated = await product.findById(data.productId).then(product => {
		product.orderee.push({userId: data.userId});

		return product.save().then((product, error) => {
			if(error){
				return false
			} else {
				return true
			}
		})
	})

	if(isUserUpdated && isProductUpdated) {
		return true
	} else {
		return false
	}
}

module.exports.getMyOrders = (reqBody) => {
	return user.findById(reqBody).then(result => {
		console.log(result.orders)
		return result.orders
	})
}