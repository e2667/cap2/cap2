const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const port = 4000;
const userRoutes = require("./routes/userRoutes")
const productRoutes = require("./routes/productRoutes")

const app = express();

mongoose.connect("mongodb+srv://admin:admin123@course-booking.4kkyq.mongodb.net/Capstone2?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection;
db.on('error', () => console.error.bind(console, 'Connection Error'));
db.once('open', () => console.log('Now connected to MongoDB Atlas'));

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use("/users", userRoutes);
app.use("/products", productRoutes);


app.listen(process.env.PORT || port, () => {
	console.log(`API is now online on port ${process.env.PORT 	|| port}`)
});